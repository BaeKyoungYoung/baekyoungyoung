
# 인텔리제이 톰캣 로그 한글 깨짐 현상
- UTF-8 인코딩 설정
    + 'idea64.exe.vmoptions' 파일에 내용 추가
      > -Dfile.encoding=UTF-8

      ※ 참고   
      https://atoz-develop.tistory.com/entry/IntelliJ-%ED%86%B0%EC%BA%A3-%EC%84%9C%EB%B2%84-%EB%A1%9C%EA%B7%B8-%ED%95%9C%EA%B8%80-%EA%B9%A8%EC%A7%90%EC%9D%84-%ED%95%B4%EA%B2%B0%ED%95%98%EB%8A%94-2%EA%B0%80%EC%A7%80-%EB%B0%A9%EB%B2%95
 